#!/bin/sh

PERCENT=$1
USER=$2

cat << EOF | /usr/lib/dovecot/dovecot-lda -d $USER -o "plugin/quota=maildir:User quota:noenforcing"
From: no-reply@office.DOMAIN.TLD
Subject: your mailbox is full at ${PERCENT}
Content-Type: text/plain; charset="utf-8"

Spring cleaning time \o/
EOF
