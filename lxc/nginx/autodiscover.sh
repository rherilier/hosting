#!/bin/sh

POST_DATA=`cat`

EMAIL=`echo $POST_DATA | grep EMailAddress | sed -e 's,^.*<EMailAddress>\([^<]\+\)</EMailAddress>.*,\1,'`

printf "Content-Type: application/xml\r\n"
printf "\r\n"

cat <<EOF
<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">
  <Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">
    <Account>
      <AccountType>email</AccountType>
      <Action>settings</Action>
      <Protocol>
        <Type>IMAP</Type>
        <Server>mail.office.DOMAIN.TLD</Server>
        <Port>993</Port>
        <DomainRequired>off</DomainRequired>
        <LoginName>$EMAIL</LoginName>
        <SPA>off</SPA>
        <SSL>on</SSL>
        <AuthRequired>on</AuthRequired>
      </Protocol>
      <Protocol>
       <Type>SMTP</Type>
        <Server>mail.office.DOMAIN.TLD</Server>
        <Port>465</Port>
        <DomainRequired>off</DomainRequired>
        <LoginName>$EMAIL</LoginName>
        <SPA>off</SPA>
        <Encryption>TLS</Encryption>
        <AuthRequired>on</AuthRequired>
        <UsePOPAuth>off</UsePOPAuth>
        <SMTPLast>off</SMTPLast>
      </Protocol>
    </Account>
  </Response>
</Autodiscover>
EOF
