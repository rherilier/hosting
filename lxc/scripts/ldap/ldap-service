#!/bin/sh

usage()
{
    echo "usage: `basename $0` action ..."
    echo
    echo "  list"
    echo "      list all services"
    echo "  add service"
    echo "      add a new service"
    echo "  users service"
    echo "      list all user of a service"
    echo "  adduser username service"
    echo "      add an user to a service"
    echo "  deluser username service"
    echo "      remove an user from a service"
}

test $# -lt 1 && usage $0 && exit 1

ROOTDIR=`readlink -f $0 | xargs dirname`
. $ROOTDIR/ldap-common

##############################################################################
# list all LDAP services
#
service_list()
{
    ldap_search_service "*" cn | grep ^cn: | awk '{print $2}' | sort -u
}

##############################################################################
# list all users of a service
# $1: service name
#
service_list_users()
{
    test $# -ne 1 && echo "service_list_users service" && exit 1

    GNAME=$1

    ldap_search_service "$GNAME" member | grep "^member: uid=" | sed -e 's/^[^=]*=\([^,]*\).*/\1/' | sort -u
}

##############################################################################
# add a service
# $1: service name
# $2: description
#
service_add()
{
    test $# -ne 2 && echo "service_add service description" && exit 1

    GNAME=$1
    GDESC="$2"
    L_G_DN="cn=$GNAME,$L_SRV_DN"

    if service_list | grep -q "$GNAME"
    then
	echo "service $GNAME already exists"
	exit 1
    fi

    cat > $LUFILE <<EOF
dn: $L_G_DN
cn: $GNAME
description: $GDESC
objectClass: groupOfNames
member: $L_ADM_DN
EOF
    ldap_add -f "$LUFILE"
}


##############################################################################
# remove a service
# $1: service name
#
service_del()
{
    test $# -ne 1 && echo "service_del service" && exit 1

    GNAME=$1
    L_G_DN="cn=$GNAME,$L_SRV_DN"

    if service_list | grep -q "$GNAME"
    then
	cat > $LUFILE <<EOF
dn: $L_G_DN
changetype: delete
EOF
	ldap_modify -f "$LUFILE"
    else
	echo "service $GNAME does not exist"
	exit 1
    fi
}

##############################################################################
# permit an LDAP user to access to a given service
# $1: username
# $2: service
#
service_add_user()
{
    test $# -ne 2 && echo "service_add_user username service" && exit 1

    UNAME=$1
    GNAME=$2

    ldap_search_user $UNAME 2>&1 > /dev/null
    if test $? -ne 0
    then
	echo "user $UNAME does not exist"
	exit 1
    fi

    L_U_DN="uid=$UNAME,$L_USR_DN"
    L_G_DN="cn=$GNAME,$L_SRV_DN"

    if ldap_search_service "$GNAME" member | grep -qi "^member: $L_U_DN"
    then
	echo "user $UNAME already allowed to use $GNAME"
	exit 1
    fi

    cat > $LUFILE <<EOF
dn: $L_G_DN
changetype: modify
add: member
member: uid=$UNAME,$L_USR_DN
EOF
    ldap_modify -f "$LUFILE"
}

##############################################################################
# revoke an LDAP user access from a given service
# $1: username
# $2: service
#
service_del_user()
{
    test $# -ne 2 && echo "service_del_user username service" && exit 1

    UNAME=$1
    GNAME=$2

    if ! ldap_search_user $UNAME
    then
	echo "user $UNAME does not exist"
	exit 1
    fi

    L_U_DN="uid=$UNAME,$L_USR_DN"
    L_G_DN="cn=$GNAME,$L_SRV_DN"

    if ldap_search_service "$GNAME" member | grep -qi "^member: $L_U_DN"
    then
	cat > $LUFILE <<EOF
dn: $L_G_DN
changetype: modify
delete: member
member: $L_U_DN
EOF
	ldap_modify -f "$LUFILE"
    else
	echo "user $UNAME already disallowed to use $GNAME"
	exit 1
    fi

}

##############################################################################
# main

case "$1" in
    list)
	service_list
	;;
    add)
	shift
	service_add "$@"
	;;
    del)
	shift
	service_del "$@"
	;;
    users)
	shift
	service_list_users $@
	;;
    adduser)
	shift
	service_add_user $@
	;;
    deluser)
	shift
	service_del_user $@
	;;
    *)
	usage
	exit 1
	;;
esac

