#!/bin/sh

set -e

ROOTDIR=`readlink -f $0 | xargs dirname`

## Stopping all services

systemctl stop nginx
systemctl stop gitlab

## Restoring configuration, dumps, and log files

rsync -a --include-from="$ROOTDIR/gitlab-files" /vol/backup /

## Preparing the GitLab restoration (only the last one backup is required)

ls -1ct /run/gitlab/backups/* | head -n -1 | xargs rm

## Doing the GitLab restoration

sudo -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV=production && bundle exec rake gitlab:backup:restore'

## Enable GitLab NGINX proxy

ln -s /etc/nginx/sites-available/gitlab.office.DOMAIN.TLD /etc/nginx/sites-enabled/

## Restarting all services

systemctl start gitlab
systemctl start gitlab-sidekiq
systemctl start nginx
