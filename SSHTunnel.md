# SSH Tunnel

## LXC connection from local

```shell
user@host ~$ ssh -C -L 1234:CONTAINER:CONTAINTER_PORT user1@office.DOMAIN.TLD
```

Sample with GitLab:

```shell
user1@msi63 ~$ ssh -C -L 1234:gitlab.office.local:80 user1@office.DOMAIN.TLD
```

## Documentation

[SSH tunner local and remote port forwarding explaned with examples](https://blog.trackets.com/2014/05/17/ssh-tunnel-local-and-remote-port-forwarding-explained-with-examples.html)

[Créer des tunnes SSH](https://blog.sodifrance.fr/creer-des-tunnels-ssh/)

[Simplify your like with an SSH config file](https://nerderati.com/2011/03/17/simplify-your-life-with-an-ssh-config-file/)
