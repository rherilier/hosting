
# LDAP

## Schema

* dc=local
  * dc=office
    * cn=admin: the office.local admin account
    * ou=people: user accounts
	  * uid=$UNAME
	* ou=service: group accounts
	  * cn=$GNAME
	* ou=system: system accesses
	  * cn=reader: RO
	  * cn=writer: RW (may be useless)

## Installation

All commands are to be done in the container.

```shell
# aptitude install slapd ldap-utils ldapscripts rsyslog logrotate
```

Enter a password for the nodomain LDAP admin. Enter what you want, this domain will be removed.

Reconfigure the package to create the wanted domain.

```shell
# dpkg-reconfigure slapd
```

and set:

* Omit OpenLDAP server configuration? No
* DNS domain name: office.local
* Organization name? Office
* Administrator password: a password you have to keep in mind (AND WRITE)
* Confirm password: twice
* Database backend to use: MDB
* Do you want the database to be removed when slapd is purged? Yes
* Move old file in /var/lib/ldap: Yes

edit `/etc/ldap/ldap.conf` to have:

> BASE dc=office,dc=local
> URI  ldapi:///

A little check:

```shell
# ldapsearch -xLLL
dn: dc=office,dc=local
objectClass: top
objectClass: dcObject
objectClass: organization
o: Office
dc: office

dn: cn=admin,dc=office,dc=local
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: admin
description: LDAP administrator
```

Finally, copy password files:

```shell
cp /vol/config/ldappwd-* /root
```

## Configuration

### Logfile

Tell rsyslog it has a new logfile to handle.

```shell
# cp /vol/config/ldap/slapd.conf /etc/rsyslog.d/slapd.conf
# systemctl restart rsyslog
# cp /vol/config/ldap/openldap /etc/logrotate.d
# systemctl restart logrotate
```

Activate the log in `slapd`:

```shell
# ldapmodify -Y external -H ldapi:/// -f /vol/config/log.ldif
```

### Overlays

#### MemberOf

To permit to add users in a group to give access to a service:

```shell
# ldapadd -Y external -H ldapi:/// -f /vol/config/memberof_act.ldif
# ldapadd -Y external -H ldapi:/// -f /vol/config/memberof_conf.ldif
```

#### Reference integrity

To do integrity check when updating the LDAP database:

```shell
# ldapadd -Y external -H ldapi:/// -f /vol/config/refint_act.ldif
# ldapadd -Y external -H ldapi:/// -f /vol/config/refint_conf.ldif
```

#### Audit

To add auditing:

```shell
# ldapadd -Y external -H ldapi:/// -f /vol/config/audit_act.ldif
# ldapadd -Y external -H ldapi:/// -f /vol/config/audit_conf.ldif
```

```shell
# mkdir /var/log/openldap
# chmod 755 /var/log/openldap
# touch /var/log/openldap/auditlog.ldif
# chmod 755 /var/log/openldap/auditlog.ldif
# chown -R openldap:openldap /var/log/openldap
```

#### Attributes uniqueness

To make sure uid, user email, etc. are unique:

```shell
# ldapadd -Y external -H ldapi:/// -f /vol/config/unique_act.ldif
# ldapadd -Y external -H ldapi:/// -f /vol/config/unique_conf.ldif
```

#### Mail info

To have mail aliases through `mailAlias` and a lock on mail account through `mailActive`:

```shell
# ldapadd -Y external -H ldapi:/// -f /vol/config/mailinfo.ldif
```

and index them (and mail OID too):

```shell
# ldapadd -Y external -H ldapi:/// -f /vol/config/dbindex.ldif
```

#### Password policy

To have restriction on password:

the `ppolicy` schema is not included by default, it also has to be added manually.

```shell
# ldapadd -Q -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/ppolicy.ldif
# ldapadd -Y external -H ldapi:/// -f /vol/config/ppolicy_act.ldif
# ldapadd -Y external -H ldapi:/// -f /vol/config/ppolicy_conf.ldif
```

### add organisationUits

people/service/system

```shell
# /vol/scripts/ldap-cmd add -f /vol/config/OU.ldif
```

### domain configuration

#### Password policy

For *ppolicy*, we'll not use local command but our own scripts.

```shell
# /vol/scripts/ldap-cmd add -f /vol/config/ppolicy_def.ldif
```

#### RO/RW Accessors

```shell
# /vol/scripts/ldap-cmd add -f /vol/config/viewer.ldif
# /vol/scripts/ldap-cmd add -f /vol/config/writer.ldif
# ldapadd -Y external -H ldapi:/// -f /vol/config/admin-access.ldif
```

#### ACL

To remove anonymous accesses and use reader/writer/authorized user/admin accesses:

```shell
# ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f /vol/config/acl.ldif
```

## Maintenance

You have to be attached to the ldap container:

```shell
user@local ~$ ssh office
user@remote ~$ sudo /vol/lxc/container-attach ldap
```

### System

* enabling: `sudo systemctl enable slapd`
* starting: `sudo systemctl start slapd`
* stopping: `sudo systemctl stop slapd`
* configuration check: none
* configuration reload: `sudo systemctl restart slapd`

### Users

#### List defined users

```shell
# /vol/scripts/ldap-user list
```

#### Show information about a given user

```shell
# /vol/scripts/ldap-user show USERNAME
```

#### Add an new user

```shell
# /vol/scripts/ldap-user add USERNAME
# /vol/scripts/ldap-user pwd USERNAME
```

#### Removing a user

```shell
# /vol/scripts/ldap-user del USERNAME
```

#### Locking an user

To freeze their access.

```shell
# /vol/scripts/ldap-user lock USERNAME
```

#### Unlocking an user

To get its privileges back.

```shell
# /vol/scripts/ldap-user unlock USERNAME
```

#### Resetting an user password

If it want to change it.

```shell
# /vol/scripts/ldap-user pwd USERNAME
```

### Alias

#### List user alias set

```shell
# /vol/scripts/ldap-alias list USERNAME
```

#### Adding alias for a given user

```shell
# /vol/scripts/ldap-alias add USERNAME ALIAS
```

#### Removing alias from a given user

```shell
# /vol/scripts/ldap-alias del USERNAME ALIAS
```

### Services

#### Add a new service

```shell
# /vol/scripts/ldap-service add SRVNAME
```

#### Removing an obsolete service

```shell
# /vol/scripts/ldap-service del SRVNAME
```

#### Granting user access to a service

```shell
# /vol/scripts/ldap-service adduser USERNAME SRVNAME
```

#### Revoking user access to a service

```shell
# /vol/scripts/ldap-service deluser USERNAME SRVNAME
```

#### List others commands

```shell
# /vol/scripts/ldap-service
```

### Backup

```shell
# /vol/scripts/ldap-backup
```

### Restore

```shell
# /vol/scripts/ldap-restore
```

### Extra commands

#### List locked users

```shell
# /vol/scripts/ldap-cmd search -b ou=people,dc=office,dc=local '(&(uid=*)(objectclass=pwdpolicy)(pwdAccountLockedTime=*))'
```

#### List unlocked users

Note the `!` in the request.

```shell
# /vol/scripts/ldap-cmd search -b ou=people,dc=office,dc=local '(&(uid=*)(objectclass=pwdpolicy)(!(pwdAccountLockedTime=*)))'
```

### List active email account

```shell
# /vol/scripts/ldap-cmd search -b ou=people,dc=office,dc=local '(&(uid=*)(mailActive=TRUE))'
```

## Documentation

[OpenLDAP setup (Debugo)](https://blog.debugo.fr/openldap-installation-configuration/)

[LDAP setup (Debugo)](https://blog.debugo.fr/serveur-messagerie-ldap/)
