
# GitLab Runners

## System configuration

### Debian

#### System configuration

first, add the Debian repositories for GitLab.

```shell
# apt-get install dirmngr
# cat > /etc/apt/sources.list.d/sid.list << EOF
deb http://ftp.fr.debian.org/debian/ sid main contrib non-free
EOF
# cat > /etc/apt/preferences.d/00pinning << EOF
Package: *
Pin: release n=buster
Pin-priority: 900

Package: *
Pin: release a=unstable
Pin-priority: 100
EOF
# apt-get update
```

Some  missing GPG public keys.

```shell
# apt-get update
```

#### Installation

```shell
# apt-get install gitlab-runner
# gitlab-runner register
```

gitlab-ci coordinator: http://gitlab.office.DOMAIN.TLD
gitlab-ci token: the one given by GitLab
gitlab-ci description: a description
gitlab-ci tags: add the system name: debian, windows, etc.
gitlab-ci executor: shell

```shell
# vi /etc/gitlab-runner/config.toml
```

set concurrent to 4 instead of 1.

```shell
# mkdir /home/gitlab-runner
# chown gitlab-runner: /home/gitlab-runner/
# chmod 700 /home/gitlab-runner/
# usermod -d /home/gitlab-runner gitlab-runner
# usermod -s /bin/bash gitlab-runner
# su - gitlab-runner
$ mkdir ~/.ssh
$ chmod 700 ~/.ssh
$ ssh-keygen
## set no keypass
$ ssh-keyscan gitlab.office.DOMAIN.TLD >> ~/.ssh/known_hosts
$ chmod 600 ~/.ssh/known_hosts
## quit
# vi /home/gitlab-runner/.ssh/id_rsa.pub
```

Copy the *gitlab-runner* user public SSH key and add it as a public deploy key in GitLab *Admin Area*/*Deploy Keys*, click on the *New deploy key` button; use the container name as key name.


### Windows

On the Windows host:

* install Git as a global program;
* in *Settings*, go to *Apps*/*Apps and Features*/*Manage Optional Features*;
* search for "OpenSSH";
* install *OpenSSH Client*;
* create the directory "C:\GitLab-Runner".
* create a new user named 'gitlab-ci';
* give it a password;
* log in as gitlab-ci (to create its home directory);
* open a console;
* `mkdir ~/.ssh`;
* `cd ~/.ssh`;
* `ssh-keygen` and do not set any password;
* copy the public key (*id_dsa.pub*) content.

Open the GitLab website:

* in *Admin Area*/*Deploy keys*;
* create a new entry and paste the previously created public key;
* in *Admin Area*/*Overview*/*Runners*;
* in the top-right corner, click on *install GitLab Runner*;
* in *Binary* section, click on *install on Windows*;
* download the binary in *C:\GitLab-Runner*;

On the Windows host:

* follow the instructions with the *amd64* version to register the runner;
* in a console, in *C:\GitLab-Runner*:
  * `mkdir build`;
  * `.\gitlab-runner-....exe install --config C:\GitLab-Runner\config.toml --working-directory C:\GitLab-Runner\build`.
  * `cd ..`
  * `C:\path\to\git\bin\git.exe git clone gitlab@gitlab.ofice.DOMAIN.TLD:DOMAIN/test`
  * type 'yes'.

gitlab-ci coordinator: https://gitlab.office.DOMAIN.TLD
gitlab-ci token: the one given by GitLab
gitlab-ci description: a description
gitlab-ci tags: windows
gitlab-ci executor: shell

* Run the Windows service manager;
* find the gitlab-runner service;
* *Settings*;
* set *gitlab-ci* as user the service will run under;
* start (or restart) it.

CMake must be able to find any dependencies:
* C++ compiler;
* Qt;
* VTK;
* ITK;
* etc.

### Realtime OS

#### QNX

As long QNX is not supported as Go language target, a GitLab runner can not be installed as in.

But using an SSH executor permits to do builds/check/whatever. We need to have a dedicated runner to forward the jobs and an SSH access to a QNX host.

It seems that QNX does not provide bash but GitLab runner support sh. `.gitlab-ci.yml` scripts must be written for `sh`, not `bash`.

TODO: finnish it.

## Documentation

### GitLab runner

[A Complete Guide to Usage of ‘usermod’ command](https://www.tecmint.com/usermod-command-examples/)

[GitLab documentation about SSH keys](https://docs.gitlab.com/ce/ci/ssh_keys/)

[Ho do I enable cloning over SSH for a GitLab runner (SO)](https://stackoverflow.com/questions/39208420/how-do-i-enable-cloning-over-ssh-for-a-gitlab-runner)

but need update (RUNNER_TOKEN does not exist anymore)...

[GitLab runner advanced configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html)

[GitLab documentation about shell executor](https://docs.gitlab.com/runner/shells/index.html)

[GitLab-CI file of Radiance](https://github.com/NREL/Radiance/blob/master/.gitlab-ci.yml)

### Windows

[OpenSSH installation under Windows](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_install_firstuse)

[OpenSSH key managment under Windows](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement)

https://struanclark.com/blog/adding-ssh-key-windows-system-account-git

### QNX

[GitLab feature request for QNX support](https://gitlab.com/gitlab-org/gitlab-runner/issues/2614)
