/
# Mail server

## Installation

All commands are to be done in the container.

First make sure to have ssl-cert.

```shell
# apt-get install ssl-cert
```

### Postfix (SMTP)

Postfix installation:

```shell
# apt-get install postfix postfix-ldap ca-certificates
# adduser postfix ssl-cert
```

and set:
* Internet site
* mail.office.DOMAIN.TLD

### Dovecot (IMAP)

Dovecot installation:

```shell
# apt-get install dovecot-core dovecot-imapd dovecot-ldap dovecot-lmtpd
# adduser dovecot ssl-cert
```

### DNS record

Make sure that your mail server hostname can be resolved.

There is a limitation in the DNS specification which disallow to use an CNAME record as MX. The mail server has also to be defined as an A (or AAAA) record.

For mail auto discovery, add the next SRV records:

```
_smtps._tcp.office             IN SRV    1 1 465 mail.office.DOMAIN.TLD.
_imaps._tcp.office             IN SRV    1 1 993 mail.office.DOMAIN.TLD.

_autodiscover._tcp.office      IN SRV    1 1 443 autoconfig.office.DOMAIN.TLD.
autoconfig.office              IN CNAME  office.DOMAIN.TLD.
```

No enough... A autoconfig service should be installed...


### SSL Certificate

Certificate bot (former letsencrypt) installation:

```shell
# apt-get install certbot
# certbot certonly --standalone -d mail.office.DOMAIN.TLD
# chown root:ssl-cert /etc/letsencrypt/archive/mail.office.DOMAIN.TLD/*.pem
# chmod 640 /etc/letsencrypt/archive/mail.office.DOMAIN.TLD/*.pem
```

and use *sysadm@DOMAIN.TLD* as contact mail address.

Next, DH (Diffie-Hellman) keys have to be created for TLS encryption.

```shell
# mkdir /etc/ssl/private/mail.office.DOMAIN.TLD
# openssl dhparam -out /etc/ssl/private/mail.office.DOMAIN.TLD/dh512.pem 512
# openssl dhparam -out /etc/ssl/private/mail.office.DOMAIN.TLD/dh2048.pem 2048
# openssl dhparam -out /etc/ssl/private/mail.office.DOMAIN.TLD/dhparam4096.pem 4096
# chmod 644 /etc/ssl/private/mail.office.DOMAIN.TLD/dh{512,2048,4096}.pem
```

## Configuration

The mail server will provide 4 accesses:
* SMTP for LXC containers informations reporting;
* SMTPS for not so modern MUA;
* IMAPS for mailboxes accesses.

### Postfix

```shell
# cp /vol/config/postfix-main.cf /etc/postfix/main.cf
```

add the following line after the smtp one:

```
smtp      inet  n       -       y       -       -       smtpd
smtps     inet  n       -       y       -       -       smtpd -o smtpd_tls_security_level=encrypt -o smtpd_tls_wrappermode=yes
```

### LDAP

```shell
# cp /vol/config/postfix/virtual_mailbox.cf /etc/postfix/ldap
# chmod -R 640 /etc/postfix/ldap/
# chown :postfix /etc/postfix/ldap/*
# systemctl reload postfix
```

### Dovecot

#### virtual user

```shell
# adduser --disabled-password --disabled-login --gecos "" --shell "" vmail
# chmod 770 /home/vmail
```

#### Configuration

```shell
# cd /etc/dovecot/conf.d
# rm 10-director* 10-tcpwrapper* 90-acl* 90-plugin* auth-*

# cd /etc/dovecot
# rm dovecot-dict-* dovecot-sql.conf.ext dovecot-ldap.conf.ext

# cp /vol/config/dovecot/dovecot.conf /etc/dovecot/
# cp /vol/config/dovecot/dovecot-ldap-* /etc/dovecot/
# chmod 600 /etc/dovecot/dovecot-ldap-*

# cp /vol/config/dovecot/[1-9]*.conf /etc/dovecot/conf.d

# cp /vol/config/dovecot/quota.sh /etc/dovecot/
# chmod +x /etc/dovecot/quota.sh
# chown vmail /etc/dovecot/quota.sh

# systemctl reload dovecot
```

### IP routing

port 25 is not publicly open as it is only used for containers. On the host server:

```shell
$ sudo iptables -t nat -A PREROUTING -p tcp -m multiport --dports 465 -j DNAT --to-destination 10.0.3.3
$ sudo iptables -t filter -A FORWARD -p tcp -m multiport --dports 465 -j ACCEPT -d 10.0.3.3
$ sudo iptables -t nat -A PREROUTING -p tcp -m multiport --dports 993 -j DNAT --to-destination 10.0.3.3
$ sudo iptables -t filter -A FORWARD -p tcp -m multiport --dports 993 -j ACCEPT -d 10.0.3.3
$ sudo iptables-save | sudo tee /etc/iptables/rules.v4
$ sudo ip6tables-save | sudo tee /etc/iptables/rules.v6
```

### Certificate

```shell
# certbot --standalone certonly -d mail.office.DOMAIN.TLD
```

and answer. Give sysadm@DOMAIN.TLD as contact mail address.

## Maintenance

You have to be attached to the mail container:

```shell
user@local ~$ ssh office
user@remote ~$ sudo /vol/lxc/container-attach mail
```

### System

#### Dovecot

* enabling: `sudo systemctl enable dovecot`
* starting: `sudo systemctl start dovecot`
* stopping: `sudo systemctl stop slapd`
* configuration check: none
* configuration reload: `sudo systemctl reload dovecot`


#### Postfix

* enabling: `sudo systemctl enable postfix`
* starting: `sudo systemctl start postfix`
* stopping: `sudo systemctl stop slapd`
* configuration check: none
* configuration reload: `sudo systemctl reload postfix`


### Backup

```shell
# /vol/scripts/mail-backup
```

### Restore

```shell
# /vol/scripts/mail-restore
```

## Documentation

[Postfix setup (Debugo)](https://blog.debugo.fr/serveur-messagerie-postfix/)

[Dovecot setup (Debugo)](https://blog.debugo.fr/serveur-messagerie-dovecot/)

[Postfix optimization (Debugo)](https://blog.debugo.fr/serveur-messagerie-optimisation-postfix/)

[Postfix connection with LDAP](https://blog.tnyc.me/postfix-with-ldap)

[Dovecot connection with LDAP](https://blog.tnyc.me/dovecot-with-ldap)

[Comment auto configuration comptes messagerie thunderbird autres (Scoffoni)](https://philippe.scoffoni.net/comment-auto-configuration-comptes-messagerie-thunderbird-autres/)
