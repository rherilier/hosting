
# DNS

The goal of setting up our own DNS server is to ease access to different services.

## Installation

```shell
$ sudo aptitude install bind9
```

## Configuration

edit `/etc/bind/named.conf.options` and after the comment about root key, add:

```
	dnssec-enable yes;
	dnssec-validation yes;

	auth-nxdomain no;
	listen-on {
		127.0.0.1;
		192.168.1.2;
	};

	forwarders {
		192.168.1.1;
	};

	recursion yes;
	allow-recursion {
        127.0.0/8;
        192.168.1.0/24;
    };

	max-ncache-ttl 0;
	version none;
```

edit `/etc/bind/named.conf` and add the line:

```
include "/etc/bind/named.conf.log";
```

create the file `/etc/bind/named.conf.log` withe the content:

```
logging {
    channel default_file {
        file "/var/log/named/default.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel general_file {
        file "/var/log/named/general.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel database_file {
        file "/var/log/named/database.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel security_file {
        file "/var/log/named/security.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel config_file {
        file "/var/log/named/config.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel resolver_file {
        file "/var/log/named/resolver.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel xfer-in_file {
        file "/var/log/named/xfer-in.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel xfer-out_file {
        file "/var/log/named/xfer-out.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel notify_file {
        file "/var/log/named/notify.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel client_file {
        file "/var/log/named/client.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel unmatched_file {
        file "/var/log/named/unmatched.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel queries_file {
        file "/var/log/named/queries.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel network_file {
        file "/var/log/named/network.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel update_file {
        file "/var/log/named/update.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel dispatch_file {
        file "/var/log/named/dispatch.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel dnssec_file {
        file "/var/log/named/dnssec.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };
    channel lame-servers_file {
        file "/var/log/named/lame-servers.log" versions 3 size 5m;
        severity dynamic;
        print-time yes;
    };

    category default { default_file; };
    category general { general_file; };
    category database { database_file; };
    category security { security_file; };
    category config { config_file; };
    category resolver { resolver_file; };
    category xfer-in { xfer-in_file; };
    category xfer-out { xfer-out_file; };
    category notify { notify_file; };
    category client { client_file; };
    category unmatched { unmatched_file; };
    category queries { queries_file; };
    category network { network_file; };
    category update { update_file; };
    category dispatch { dispatch_file; };
    category dnssec { dnssec_file; };
    category lame-servers { lame-servers_file; };
};

```

edit `/etc/bind/named.conf.local`, uncomment `include "/etc/bind/zones.rfc1918";` and add:

```
zone "office.local" {
	type master;
	file "/etc/bind/master/db.office.local";
};

zone "1.168.192.in-addr.arpa" {
	type master;
	file "/etc/bind/master/db.1.168.192";
};
```

replace `office.local` by the wanted domain, and `1.168.192` by the wanted subnet.

create the `/etc/bind/master` directory:

```shell
$ mkdir /etc/bind/master
```

edit the file `/etc/bind/master/db.office.local` and write:

```
$TTL	3600
@       IN      SOA     server.office.local. root.office.local. (
                        2019050201    ; serial
                                1H    ; refresh
                               10M    ; retry
                                1D    ; expire
          	                    1H )  ; negative cache TTL
;
@       IN      NS      server.office.local.
@       IN      MX      10 server.office.local.

router  IN      A       192.168.1.1
server  IN      A       192.168.1.2
```

edit the file `/etc/bind/master/db.1.168.192` and write:

```
$TTL    3600
@       IN      SOA     server.office.local. root.office.local. (
                        2019050201   ; serial
                                1H   ; refresh
                               10M   ; retry
                                1D   ; expire
                                1H ) ; negative cache TTL
;
@       IN      NS      server.office.local.

1       IN      PTR     router.office.local.
2       IN      PTR     server.office.local.
```

replace `office.local` by the wanted domain, `1.168.192` by the wanted subnet, and set a *serial* corresponding to your configuration date.


## Maintenance

* enabling: `sudo systemctl enable bind9`
* starting: `sudo systemctl start bind9`
* stopping: `sudo systemctl stop bind9`
* configuration check: `sudo named-checkconf -z`
* configuration reload: `sudo systemctl reload bind9`

log files: `/var/log/named/*`

## Updates

To add, remove, or change host or service:
* edit `/etc/bind/master/db.office.local` and `/etc/bind/master/db.1.168.192` symmetrically;
* update the *serial* in those file (use the format YYYYMMDDNN using the current date and iteration 2 digits.

check the configuration, and force a reload.

## Client configuration

### Internet gateway

Change the DNS address in the DHCP configuration if possible.
on Liveboxes, it can not be changed.

### Windows

TODO: look at Wi-Fi connection parameters.

### Network Manager

* right click on the network manager notification icon;
* click on *modify connection*;
* click on the wanted connection;
* click on the IPv4 tab;
* switch method to "Automatic (DHCP) addresses only"
* add *192.168.1.2* in the DNS server;
* add *office.local* in the *additional search domain*;
* save
* left click on the network manager notification icon;
* disconnect
* reconnect

### resolvconf.conf

edit `/etc/resolvconf.conf` and add a line

```
search_domains office.local
```

## Documentation

[Debian wiki about bind)[https://wiki.debian.org/Bind9]
