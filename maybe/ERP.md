
ERP
===

# Installation

```shell
$ sudo aptitude install oca-core
```

# Configuration

TOCHECK: edit `/etc/odoo/odoo.conf` and set password for the database.

Add plugins for:
* vacancy
* dealers
* customers
* ...

Odoo is stated on localhost:8069, need a DNS entry and a NGINX reverse.

# Maintenance

* enabling: `sudo systemctl enable oca-core`
* starting: `sudo systemctl start oca-core`
* stopping: `sudo systemctl stop oca-core`

log file: `/var/log/odoo/odoo-server.log`

# Updates

