
# OVH DNS

## DNS terminnology

SOA (Start of Authority)::
The entry point of a DNS zone.

A::
The IPv4 of the subdomain.

AAAA:
The IPv6 of the subdomain.

MX::
The mail server

NS (Name Server)::
the parent DNS.

CNAME (Canonical Name)::
An alias for an other A or AAAA record.

SRV (Services)::
To declare services like smtps, imaps, or autodiscover.

TXT::
Free information.

CAA (Cerfificate Authority Authorization)::
To tell which certificate authorities are allowed to issue a certificate for a domain.


## OVH

OVH does not activate DNSSEC by default, active it.

We can not define subzone (to separate `DOMAIN.TLD` from `office.DOMAIN.TLD`). But using subdomains amount to the same thing.

SRV and MX records must not use a CNAME record but an A one due to a limitation in DNS specs.
_autoconfig must not use a CNAME too...

## DOMAIN.TLD

This domain must be manage by OVH. Some conflicts occurred because Microsoft were used to manage
it (but OVH continued to do it too). Now, only OVH manage it.

This domain must point to the default OVH services: HTTP, FTP, etc.

This domain must point to the Microsoft mail server too: MX, SPF, SIP, etc.

## office.DOMAIN.TLD

This subdomain must point to our services, hosted on an OVH VPS:

* mail server certificate update;
* SMTPS/IMAPS;
* autoconfig/autodiscover;
* GitLab.

# Whole configuration

The following lines have been reordered to ease readability.

```
$TTL 3600
@	IN SOA dns18.ovh.net. tech.ovh.net. (2019060711 86400 3600 3600000 300)
                                 IN NS     ns18.ovh.net.
                                 IN NS     dns18.ovh.net.
                                 IN A      IPv4_3
                                 IN AAAA   IPv6_1

								 IN MX 1   DOMAIN-TLD01i.mail.protection.outlook.com.
                             600 IN TXT    "v=spf1 include:spf.protection.outlook.com -all"
                             600 IN TXT    "mscid=BASE64_HASH"
                                 IN TXT    "1|www.DOMAIN.TLD"

ftp                              IN CNAME  DOMAIN.TLD.

www                              IN A      IPv4_3
www                              IN AAAA   IPv6_1
www                              IN MX 1   DOMAIN-TLD01i.mail.protection.outlook.com.
www                              IN TXT    "l|fr"
www                              IN TXT    "3|welcome"

lyncdiscover                     IN CNAME  webdir.online.lync.com.
sip                              IN CNAME  sipdir.online.lync.com.
autodiscover                3600 IN CNAME  autodiscover.outlook.com.
_sip._tls                        IN SRV    1 100 443 sipdir.online.lync.com.
_sipfederationtls._tcp           IN SRV    100 1 5061 sipfed.online.lync.com.
enterpriseenrollment             IN CNAME  enterpriseenrollment.manage.microsoft.com.
enterpriseregistration           IN CNAME  enterpriseregistration.windows.net.

office                           IN A      IPv4_1
office                           IN AAAA   IPv6_2
office                           IN MX 1   mail.office.DOMAIN.TLD.

mail.office                      IN A      IPv4_1
mail.office                      IN AAAA   IPv6_2
mail.office                      IN CAA    128 issue "letsencrypt.org"

_smtps._tcp.office               IN SRV    1 1 465 mail.office.DOMAIN.TLD.
_imaps._tcp.office               IN SRV    1 1 993 mail.office.DOMAIN.TLD.

autoconfig.office                IN A      IPv4_1
autoconfig.office                IN CAA    128 issue "letsencrypt.org"

_autodiscover._tcp.office        IN SRV    1 1 80 autoconfig.office.DOMAIN.TLD.

gitlab.office                    IN A      IPv4_1
gitlab.office                    IN AAAA   IPv6_2
gitlab.office                    IN CAA    128 issue "letsencrypt.org"
```

# Original OVH DNS configuration

It's a backup before moving DNS authority to OVH
```
$TTL 3600
@	IN SOA dns18.ovh.net. tech.ovh.net. (2019052806 86400 3600 3600000 300)
                     3600 IN NS     dns18.ovh.net.
                     3600 IN NS     ns18.ovh.net.
                     3600 IN MX 1   mx1.mail.ovh.net.
                     3600 IN MX 100 mx3.mail.ovh.net.
                     3600 IN MX 5   mx2.mail.ovh.net.
                     3600 IN A      IPv4_3
                      600 IN TXT    "v=spf1 include:mx.ovh.com ~all"
                      600 IN TXT    "1|www.DOMAIN.TLD"
                      600 IN TXT    "Nom TXT Valeur TXT DUR195137E DE VIE  @  v=spf1 include:spf.protection.outlook.com -all  3600"
_autodiscover._tcp   3600 IN SRV    0 0 443 mailconfig.ovh.net.
_imaps._tcp          3600 IN SRV    0 0 993 ssl0.ovh.net.
_submission._tcp     3600 IN SRV    0 0 465 ssl0.ovh.net.
autoconfig           3600 IN CNAME  mailconfig.ovh.net.
autodiscover         3600 IN CNAME  mailconfig.ovh.net.
ftp                  3600 IN CNAME  DOMAIN.TLD.
imap                 3600 IN CNAME  ssl0.ovh.net.
mail                 3600 IN CNAME  ssl0.ovh.net.
office               3600 IN NS     dns18.ovh.net.
office               3600 IN NS     ns18.ovh.net.
office               3600 IN A      IPv4_1
pop3                 3600 IN CNAME  ssl0.ovh.net.
smtp                 3600 IN CNAME  ssl0.ovh.net.
www                  3600 IN NS     ns18.ovh.net.
www                  3600 IN NS     dns18.ovh.net.
www                  3600 IN MX 1   mx1.mail.ovh.net.
www                  3600 IN MX 100 mx3.mail.ovh.net.
www                  3600 IN MX 5   mx2.mail.ovh.net.
www                  3600 IN A      IPv4_3
www                  3600 IN TXT    "3|welcome"
www                  3600 IN TXT    "l|fr"
```
