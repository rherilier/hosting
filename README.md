
# Disclaimer

This project is old (2019), surely outdated, may have security weaknesses,
or may contain errors I forget to fix in the present documentation.

for more recent projects, look at:

* [YunoHost](https://yunohost.org);
* [HomeBox](https://github.com/progmaticltd/homebox).

or do your own way (which is much funnier;)

# Introduction

This repository centralizes any information about installing, configuring and
managing a minimalist office technical server.

# Needs

For the development/QA aspect, a forge and an automation service are mandatory.

As a forge send lots of notificiations and as some communications should be
kept private, having a private mail server should be relevant.

For user identification and authentication centralization, a directory
should be really helpful.

As the office server will be public, any connections must be secured.

# Use

## Office related configuration

See [GitLab configuration for DOMAIN](GitLabDomainConfig.md) for use in the office.

## Administrator use

See [GitLab basic administration](GitLabAdmin101.md) for basic administration.

# Server

The server will be named *office.DOMAIN.TLD* and public.

As those service are on Internet, they have to be secured :

* each service is containerized using unprivileged LXC containers;
* any connections (HTTP, SMTP, and IMAP) are secured by default using Let's Encrypt.

See [Server setup](Server.md) and [LXC setup](LXC.md) for technical details.

![server and service architecture](./network.png "Server and service architecture").

# Services

The service related documents are globally structure as:

* system setup;
* service installation;
* service configuration;
* maintenance;
* documentation.

## Directory server

To manage user information, access and authentication.

This service is managed using LDAP (Lightweight Directory Access Protocol).

This service is provided with OpenLDAP.

The LDAP server will not be public.

The LDAP container will be named *ldap*.

See [LDAP service setup](LDAD.md) for technical details.

## Mail server

to manage private communications.

This service is provided using:
* Postfix for the SMTP;
* Dovecot for the IMAP.

The mail server will be public.

The mail container will be named *mail*.

See [Mail service setup](Mail.md) for technical details.

### SMTP configuration

* server: mail.office.DOMAIN.TLD;
* port: 465;
* user name: email address;
* security: SSL/TLS;
* authentication: normal password.

### IMAP configuration

* server: mail.office.DOMAIN.TLD;
* port: 993;
* user name: email address;
* security: SSL/TLS;
* authentication: normal password.

## Forge

To centralize Git repositories, tickets, merge requests, documentation, CI/CD.

This service is provided using GitLab.

The forge server will be public.

The forge container will be named *gitlab* and publicly named *gitlab.office.DOMAIN.TLD*.

See [GitLab service setup](GitLab.md) for technical details.

## Automation slaves

To effectively do tasks like build, check, packaging.

Those services will be provided using GitLab Runner.

The automation slaves will not be public.

Having buils & co running on office workstations will be better due to the implied system load.

So, next is 

The automation slaves container will be named *builder*, *packager*, or *whatever*.

See [GitLab runner setup](GitLabRunner.md) for technical details.
