
# DOMAIN GitLab configuration

## Configuration

### create a GitLab group named Domain

*Admin Area*/*Overview*/*Groups* and click on *New groups*.

Make sure users have connected at least one time.

Add them to the *Domain* group and grant them to *Reporter*.

### Create a GitLab administrator

It is used to be the owner of the *Domain* group.

*Admin Area*/*Overview*/*Users* and click on *New user*.

Name it *gitlab-admin* with the email address *admin@office.DOMAIN.TLD* (the office administrator alias).

Set its notification level to *Disabled* to avoid spam.

Set it as *Domain* group owner.

Unset yourself from being a *Domain* group owner (as an owner can not downgrade its role).

### Add labels from software developement procedure

*Groups*/*Domain*/*Issues*/*Labels* and create labels defined in the document *000056 Sofware Developement Procedure*.

### Add projects

* *spider-sw* for the software system;
* *spider-hw* for the hardware system;
* *team* for teamwork (tasks, daily life, etc.);

All software engineers are granted to the *Developer* in the *spider-sw* project.

All hardware engineers are granted to the *Developer* in the *spider-hw* project.

Merge requests restriction has to be set for repositories which need to. Under *Groups*/*Domain*, open a new tab for the repositories and:

* in the left menu: *Settings*/*General*:
  * in section *Merge requests*;
  * check-in *Only allow merge requests to be merged if the pipeline succeeds*;
  * check-in *Only allow merge requests to be merged if all discussions are resolved*;
* in the left menu: *Settings*/*Repository*:
  * in section *Protected Branches*:
  * select *Developers + Maintainers* as roles allowed to merge requests.

Shared runners must be explictly enabled for each projects:

* in left menu: *Settings*/*General*/*CI/CD*;
* in section *Runners*;
* click on *Enable shared Runners*.
