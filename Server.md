
# Server installation

## Upgrade to buster

```shell
$ sudo aptitude update
$ sudo aptitude upgrade
$ sudo aptitude dist-upgrade
$ sudo vi /etc/apt/sources.list
# :0,$s/stretch/buster
# uncomment backports repositories
$ sudo aptitude update
$ sudo aptitude upgrade
$ sudo aptitude dist-upgrade
$ sudo poweroff --reboot
```

```shell
root@container:~# apt-get install unattended-upgrades iputils-ping less bash-completion
root@container:~# dpkg-reconfigure tzdata
# set it to Europe/Paris
root@container:~# vi /etc/apt/apt.conf.d/50unattended-upgrades
```

and uncomment to enable updates and backports (not just security updates):

```
"origin=Debian,codename=${distro_codename}-updates";
"origin=Debian,codename=${distro_codename}-proposed-updates";
...
"o=Debian Backports,a=${distro_codename}-backports,l=Debian Backports";
```

and restart unattended-upgrades:

```shell
root@container:~# systemctl restart unattended-upgrades.service
```

## System mail forwarding

After have build the mail container, the system mail forwarding can be configured. As there is
no DNS for containers access, the IP is used instead of a hostname.

```shell
$ sudo aptitude install exim4-base bsd-mailx
$ dpkg-reconfigure exim4-config
# "mail sent by smarthost; no local mail"
# "office.DOMAIN.TLD"
# "127.0.0.1 ; ::1"
# ""
# "office.DOMAIN.TLD"
# "10.0.3.3"
# "No"
# "Yes"
# "admin@office.DOMAIN.TLD"
root@container:~# systemctl reload exim4
```

## SSH configuration

```shell
$ sudo vi /etc/ssh/sshd_config
# uncomment Port 22
# and replace 22 by 2222
# add AuthenticationMethods publickey
# comment X11Forwarding on
# quit
$ sudo systemctl reload ssh
```

In your SSH client configuration, add a similar configuration (example for *~/.ssh/config*):

```
Host office
     Hostname office.DOMAIN.TLD
	 HostkeyAlias "[office.DOMAIN.TLD]:2222"
     Port 2222
     User user1
     IdentityFile ~/.ssh/ovh_user1_rsa
     AddKeysToAgent yes
```

Next, the SSH access has to be hardened against brute force connections by using fail2ban:

```shell
$ sudo aptitude install fail2ban
```

```shell
$ sudo vi /etc/fail2ban/fail2ban.d/fail2ban.local
```

```
[DEFAULT]

bantime  = 10m
findtime  = 10m
maxretry = 5

# enable jail
enabled = true

mta = mail
destemail = admin@office.DOMAIN.TLD
sendername = Fail2Ban
```

```shell
$ sudo vi /etc/fail2ban/jail.d/jail.local
```

```[DEFAULT]

# localhost, containers, host, office
ignoreip = 127.0.0.1/8 10.0.3.0/24 IPv4_1 IPv4_2

[ssh]

enabled = true
port = 2222
filter = sshd
logpath  = /var/log/auth.log
maxretry = 6
```

and restart fail2ban:

```shell
$ sudo systemctl reload fail2ban
```

## Iptables

```shell
$ sudo aptitude install iptables-persistent
# accept persistence for IPv4 and IPv6
```

Do not forget to save your iptables rules each time they are changed:

```shell
$ sudo iptables-save | sudo tee /etc/iptables/rules.v4
$ sudo ip6tables-save | sudo tee /etc/iptables/rules.v6
```

## Reverse proxy using NGINX

```shell
$ sudo aptitude install nginx
$ sudo rm /etc/nginx/sites-enabled/default
$ sudo systemctl reload nginx
```

```shell
$ sudo aptitude install certbot python-certbot-nginx ssl-cert
$ sudo mkdir -p /var/www/letsencrypt
```

## Certificate automation

To have SSL certificates, letsencrypt will be used. This service provides signed SSL certificates
for free. They are valid for 3 months and can be renewed automatically.

```shell
$ sudo certbot certonly --nginx -d SERVICE.office.DOMAIN.TLD -w /var/www/letsencrypt/
# sysadm@DOMAIN.TLD
# A
# N
# 2 (for renew)
$ sudo ln -s /etc/nginx/sites-available/SERVICE-ssl /etc/nginx/sites-enabled
$ sudo systemctl reload nginx
```

### Obtaining a temporary certificate

```shell
$ sudo certbot certonly --nginx -d SERVICE.office.DOMAIN.TLD -w /var/www/letsencrypt/ --test-cert --staging
```

Do not forget there is a limited request number.

### Listing requested certificates

```shell
$ sudo certbot certificates
```

### Removing a certificate

```shell
$ sudo certbot revoke --cert-name SERVICE.office.DOMAIN.TLD
$ sudo certbot delete --cert-name SERVICE.office.DOMAIN.TLD
```


## Mail certificate

To use certbot for certificates retrieval, the running server must be publicly accessible to
allows communications.

```shell
$ sudo cp /vol/lxc/nginx/mail /etc/nginx/sites-available/
$ sudo ln -s /etc/nginx/sites-available/mail /etc/nginx/sites-enabled
$ sudo systemctl reload nginx
```

## Mail configuration automation

```shell
$ sudo certbot certonly --nginx -d autoconfig.office.DOMAIN.TLD --w /var/www/letsencrypt/
$ sudo su
# chown root:ssl-cert /etc/letsencrypt/archive/autodiscover.office.DOMAIN.TLD/*.pem
# chmod 640 /etc/letsencrypt/archive/autodiscover.office.DOMAIN.TLD/*.pem
```

Next, DH (Diffie-Hellman) keys have to be created for TLS encryption.

```shell
$ sudo mkdir -p /etc/ssl/private/autoconfig.office.DOMAIN.TLD/
$ sudo openssl dhparam -out /etc/ssl/private/autoconfig.office.DOMAIN.TLD/dh2048.pem 2048
$ sudo chmod 644 /etc/ssl/private/autoconfig.office.DOMAIN.TLD/dh2048.pem
```

```shell
$ sudo aptitude install fcgiwrap
$ sudo mkdir -p /var/www/mail /var/www/autodiscover
$ sudo cp /vol/lxc/nginx/config-v1.1.xml /var/www/mail
$ sudo cp /vol/lxc/nginx/autodiscover.sh /var/www/autodiscover
$ sudo cp /vol/lxc/nginx/mail-autoconf /etc/nginx/sites-available/
$ sudo ln -s /etc/nginx/sites-available/mail-autoconf /etc/nginx/sites-enabled
$ sudo systemctl reload nginx
```

When all is fine, enable the reverse proxy.

```shell
$ sudo ln -s /etc/nginx/sites-available/mail-autoconf-ssl /etc/nginx/sites-enabled
$ sudo systemctl reload nginx
```

## GitLab certificate

```shell
$ sudo cp /vol/lxc/nginx/gitlab /etc/nginx/sites-available/
$ sudo ln -s /etc/nginx/sites-available/gitlab /etc/nginx/sites-enabled
$ sudo systemctl reload nginx
```

```shell
$ sudo certbot certonly --nginx -d gitlab.office.DOMAIN.TLD -w /var/www/letsencrypt/
$ sudo su
# chown root:ssl-cert /etc/letsencrypt/archive/gitlab.office.DOMAIN.TLD/*.pem
# chmod 640 /etc/letsencrypt/archive/gitlab.office.DOMAIN.TLD/*.pem
```

Next, DH (Diffie-Hellman) keys have to be created for TLS encryption.

```shell
$ sudo mkdir -p /etc/ssl/private/gitlab.office.DOMAIN.TLD/
$ sudo openssl dhparam -out /etc/ssl/private/gitlab.office.DOMAIN.TLD/dh2048.pem 2048
$ sudo chmod 644 /etc/ssl/private/gitlab.office.DOMAIN.TLD/dh2048.pem
```

## LXC

See [LXC setup](LXC.md) for container setup and usage.

## LDAP

See [LDAP service setup](LDAD.md) for LDAP server setup and usage.

## Mail

See [Mail service setup](Mail.md) for SMTP/IMAP server setup.

## GitLab

See [GitLab service setup](GitLab.md) for GitLab instance setup.

When all is fine, enable its reverse proxy.

```shell
$ sudo ln -s /etc/nginx/sites-available/gitlab-ssl /etc/nginx/sites-enabled
$ sudo systemctl reload nginx
```

### GitLab SSH access from outside

SSH can forward user authentication from the host system to a guest one, using the
AuthorizedKeysCommand option (in a *Match* block).

GitLab provides the *gitlab-shell-authorized-keys-check* command which can be
used (through *container-run*) but the username of the connection initiator can
not be retrieved and we need it...

The lone solution is to move VPS SSH port to 2222 and do a port redirection

```shell
$ sudo iptables -t nat -A PREROUTING -p tcp -m multiport --dports 22 -j DNAT --to-destination 10.0.3.4
$ sudo iptables -t filter -A FORWARD -p tcp -m multiport --dports 22 -j ACCEPT -d 10.0.3.4
$ sudo iptables-save | sudo tee /etc/iptables/rules.v4
$ sudo ip6tables-save | sudo tee /etc/iptables/rules.v6
```

## GitLab runners

See [GitLab Runner service setup](GitLabRunner.md) for builder setup.

## Server backup

```shell
$ sudo aptitude install rsnapshot
$ sudo vi /etc/rsnapshot.conf
```

In section *SNAPSHOT ROOT DIRECTORY*, set:

```
snapshot_root   /vol/rsnapshot/
```

In section *BACKUP LEVELS / INTERVALS*, set:

```
retain  daily   7
retain  weekly  4
retain  monthly 12
```

In section *BACKUP POINTS / SCRIPTS*, add:

```
backup_exec     /bin/date "+##### backup of containers started at %F %T %:z"
backup_exec     /vol/lxc/container-backup
backup_exec     /bin/date "+##### backup of host started at %F %T %:z"
backup_exec     /vol/lxc/sys-backup
backup_exec     /bin/date "+##### backup snapshot started at %F %T %:z"
backup          /vol/backup/containers/./       containers
backup          /vol/backup/host/./             host
backup_exec     /bin/date "+##### backup ended at %F %T %:z"
```

And quit.

Now, it's time to activate rsnapshot in the crontab:

```shell
$ sudo vi /etc/cron.d/rsnapshot
```

and set:

```
30      3       *       *       *               root    /usr/bin/rsnapshot daily
0       3       *       *       0               root    /usr/bin/rsnapshot weekly
30      2       1       *       *               root    /usr/bin/rsnapshot monthly
```

To run backups for:
* *daily* at 3:30 AM every day;
* *weekly* at 3:00 AM each sunday;
* *monthly* at 2:30 AM each first day of the month.

## Documentation

[Autodiscover (Github)](https://github.com/cfoellman/ISPC-resources/tree/master/guides/autodiscover)

[Autodiscover for exchange (Microsoft)](https://docs.microsoft.com/en-us/exchange/client-developer/exchange-web-services/autodiscover-for-exchange)
