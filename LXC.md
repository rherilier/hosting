
# LXC

## Before anything

All administrators must be in the group `adm`.

We'll need the following containers:

* ldap
* mail
* gitlab
* gitlab runners: builder, packager, docgen.
* ...

And they will be started in this order (see Post creation configuration)

## Installation

from local computer with scp:

```shell
user@local ~$ scp -r -F ~/.ssh/config lxc remote:/vol/lxc
```

or with rsync (do not use *-a* to preserve permissions/ACL when updating):

```shell
rsync -rltgoDvi lxc/ remote:/vol/lxc
```

file access has to be hardened on remote server (commands are not prefixed to permit a full copy/paste):

```shell
sudo chown root:adm -R /vol/lxc/
sudo chmod 770 /vol/lxc/container-*
sudo find /vol/lxc/ -type d | sudo xargs -r chmod 770
sudo find /vol/lxc/config -type f | sudo xargs -r chmod 660
sudo find /vol/lxc/scripts -type f | sudo xargs -r chmod 770
sudo find /vol/lxc -type f -a -name "*-common" | sudo xargs -r chmod 660
sudo find /vol/lxc -type f -a -name "*-files" | sudo xargs -r chmod 660
sudo chmod 775 /vol/lxc /vol/lxc/config /vol/lxc/backup /vol/lxc/scripts
```

install LXC:

```shell
user@remote ~$ sudo aptitude install lxc lxc-templates uidmap dnsmasq-base
```

The LXC bridge mode does not work with `nf_tables`, legacy iptables must set in Debian system:

```shell
user@remote ~$ sudo update-alternatives --set iptables /usr/sbin/iptables-legacy
```

For unprivileged containers, user namespaces clone must be enabled:

```shell
user@remote ~$ echo "kernel.unprivileged_userns_clone=1" | sudo tee -a /etc/sysctl.d/userns.conf
user@remote ~$ sudo sysctl --system
```

initialize the system:

```shell
user@remote ~$ sudo /vol/lxc/container-init
```

## Container creation

```shell
user@remote ~$ sudo /vol/lxc/container-create CONTAINER_NAME
```

## Post install

Start the container:

```shell
user@remote ~$ sudo /vol/lxc/container-start CONTAINER_NAME
```

And enter into it to finalize its installation:

```shell
user@remote ~$ sudo /vol/lxc/container-attach CONTAINER_NAME
root@container:~# vi /etc/apt/sources.list
```

and set to:

```
deb http://deb.debian.org/debian buster main contrib non-free
deb http://security.debian.org/debian-security buster/updates main contrib non-free
deb http://ftp.fr.debian.org/debian/ buster-updates main contrib non-free
deb http://ftp.fr.debian.org/debian/ buster-backports main contrib non-free
```

```shell
root@container:~# apt-get update
root@container:~# apt-get install unattended-upgrades iputils-ping less ca-certificates bash-completion tree rsync
root@container:~# dpkg-reconfigure tzdata
# set it to Europe/Paris
```

```shell
root@container:~# vi /etc/apt/apt.conf.d/50unattended-upgrades
```

and uncomment to enable updates and backports (not just security updates):

```
"origin=Debian,codename=${distro_codename}-updates";
"origin=Debian,codename=${distro_codename}-proposed-updates";
...
"o=Debian Backports,a=${distro_codename}-backports,l=Debian Backports";
```

and restart unattended-upgrades:

```shell
root@container:~# systemctl restart unattended-upgrades.service
```

On all but mail, system mail forwarding has to be enabled:

```shell
root@container:~# apt-get install exim4-base
root@container:~# dpkg-reconfigure exim4-config
# "mail sent by smarthost; no local mail"
# "CONTAINER_NAME.office.DOMAIN.TLD"
# "127.0.0.1 ; ::1"
# ""
# "CONTAINER_NAME.office.DOMAIN.TLD"
# "mail"
# "No"
# "Yes"
# "admin@office.DOMAIN.TLD"
root@container:~# systemctl reload exim4
```

on mail, edit `/etc/aliases` to add:

```
root@container:~# systemctl reload exim4
```

edit *~/.bashrc* and add:

```
alias ls='/bin/ls --color --classify'
alias l='/bin/ls --color --classify -l'
alias la='/bin/ls --color --classify -lsa'
alias lr='/bin/ls --color --classify -R'
alias rd='rmdir'
alias md='mkdir'
alias rm='/bin/rm -i'
alias cp='/bin/cp -i'

# enable bash completion in interactive shells
if [ -f /etc/bash_completion ]; then
            . /etc/bash_completion
fi

export HISTCONTROL=ignoreboth:erasedups
export HISTSIZE=-1
export HISTFILESIZE=-1
```

create *~/.vimrc* and add:

```
set mouse=r
syntax on
colorscheme delek
```

Proceed to its installation, see corresponding installation file.

When done, stop the container:

```shell
# quit the container
user@remote ~$ sudo /vol/lxc/container-stop CONTAINER_NAME
```

Mounted configuration directory is not needed anymore:

```shell
user@remote ~$ sudo /vol/lxc/container-login CONTAINER_NAME
adm-xxxx@remote ~# vi .local/share/lxc/CONTAINER_NAME/config
# remove line 'lxc.mount.entry = /vol/lxc/config ...'
# quit
```

An finally, the container can be started again:

```shell
user@remote ~$ sudo /vol/lxc/container-start CONTAINER_NAME
```

And voila, it's time for the next one.


## Maintenance

You have to be connected to the server host.

```shell
user@local ~$ ssh office
```

### Starting an LXC container

```shell
user@remote ~$ sudo /vol/lxc/container-start CONTAINER_NAME
```

### Stopping an LXC container

```shell
user@remote ~$ sudo /vol/lxc/container-stop CONTAINER_NAME
```

### Entering an LXC container

```shell
user@remote ~$ sudo /vol/lxc/container-attach CONTAINER_NAME
```

### Login as the LXC container

This can required to change an already created container.

```shell
user@remote ~$ sudo /vol/lxc/container-login CONTAINER_NAME
```

### Destroying an LXC container:

```shell
user@remote ~$ sudo /vol/lxc/container-stop CONTAINER_NAME
user@remote ~$ sudo /vol/lxc/container-destroy CONTAINER_NAME
```

`container-destroy` may have to be called twice to be effective. Unfortunately, it can never terminate without an error due to a `systemd` oddness. The lone solution is to terminate the command manually:

* do a `kill -9` on the blocking process and on the restarted one;
* `sudo deluser CONTAINER_NAME-admin`
* `rm -rf /home/CONTAINER_NAME-admin`

### Backup

```shell
# /vol/lxc/container-backup
```

### Restore

All containers must be rebuild from scratch and restored manually.

## Documentation

[LXC network bridge configuration (Angristan)](https://angristan.xyz/setup-network-bridge-lxc-net/#configure-the-bridge)

[LXC create unprivileged containers (SO)](https://stackoverflow.com/questions/33520980/lxc-create-unprivileged-containers)

[LXC 1.0 unprivileged containers (Stgraber)](https://stgraber.org/2014/01/17/lxc-1-0-unprivileged-containers/)

[LXC security documentation](https://linuxcontainers.org/lxc/security/)

[An unprivileged LXC container dedicated to running Wordpress (Github)](https://gist.github.com/austinjp/f19e4de86398dad6a50d)

[How to auto start unprivileged LXC containers (SO)](https://serverfault.com/questions/620709/how-to-auto-start-unprivileged-lxc-containers)
