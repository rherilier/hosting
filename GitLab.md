
# GitLab

## System configuration

### Debian's GitLab

first, add the Debian repositories for GitLab.

```shell
# apt-get install dirmngr
# cat > /etc/apt/sources.list.d/gitlab.list << EOF
deb http://fasttrack.debian.net/debian/ buster-fasttrack main contrib
deb http://fasttrack.debian.net/debian/ buster-backports main contrib
deb https://people.debian.org/~praveen/gitlab buster-backports contrib main
deb https://people.debian.org/~praveen/gitlab buster-fasttrack contrib main
EOF
# cat > /etc/apt/sources.list.d/sid.list << EOF
deb http://ftp.fr.debian.org/debian/ sid main contrib non-free
EOF
# cat > /etc/apt/preferences.d/00pinning << EOF
Package: *
Pin: release n=buster
Pin-priority: 900

Package: *
Pin: release n=buster-backports
Pin-Priority: 900

Package: *
Pin: release n=buster-fasttrack
Pin-Priority: 900

Package: *
Pin: release a=unstable
Pin-priority: 100
EOF

# apt-get update
```

Some  missing GPG public keys.

```shell
# apt-key adv --keyserver keyring.debian.org --recv-keys $PUBKEY
# apt-get update
```

### Needed servers

```shell
# apt-get install nginx openssh-server
```

## Installation

```shell
# apt-get install gitlab
```

user to run GitLab: gitlab
host fqdn: gitlab.office.DOMAIN.TLD
enable HTTPS: no
configure database with dbconfig-common? Yes
DB hostname for gitlab: localhost
enter DB password: aqwzsxedcrfv
reneter DB password: aqwzsxedcrfv

Fix some permissions:

```shell
# chown -R gitlab: /var/log/gitlab-shell
```

On 2019-07-12, GitLab fails on an *internal server error* when uploading a jpeg image in a comment. According to [GitLab issue #60853](https://gitlab.com/gitlab-org/gitlab-ce/issues/60853), `libimage-exiftool-perl` is missing as GitLab dependency.

```shell
# apt-get install libimage-exiftool-perl
```

There is no need to restart GitLab.

### Configuration

#### NGINX

make gitlab listen on port 80:

```shell
# vi /etc/nginx/sites-enabled/gitlab.office.DOMAIN.TLD
```

replace `listen localhost:80` with `listen 80`

before the *server* block, add:

```
map $request_uri $loggable {
    ~^/assets/  0;
    default     1;
}
```

change the line of `access_log` with:

```
access_log  /var/log/nginx/gitlab_access.log combined if=$loggable;
```

and restart NGINX:

```shell
# systemctl reload nginx
```

#### Logrotate

```shell
# cp /vol/config/gitlab /etc/logrotate.d/
# cp /vol/config/gitlab-shell /etc/logrotate.d/
# systemctl restart nginx
```

#### GitLab

```shell
# vi /etc/gitlab/gitlab.yml
```

under the `gitlab:` section, make the following changes:

Search for `time_zone` and add:

```
time_zone: 'Europe/Paris'
```

and:

```
container_registry: false
```

under the `gitlab-shell:` section, make the following changes:

```
receive_pack: false
```

to disable push over HTTP(S).

under the `ldap:` section, make the following changes:

```
enabled: true
label: 'Office'
host: 'ldap.office.DOMAIN.TLD'
uid: 'uid'
bind_dn: 'cn=viewer,ou=system,dc=office,dc=local'
# get the password from lxc/config/mail/postfix/virtual_mailbox.cf
password: 
active_directory: false
# TODO check if the next one is relevant
allow_username_or_email_login: true
base: 'ou=people,dc=office,dc=local'
user_filter: '(memberOf=cn=gitlab,ou=service,dc=office,dc=local)'
username: ['uid']
email:    ['mail']
```

Finally, restart GitLab (and sidekiq which sometimes gets lost):

```shell
# systemctl reload gitlab
# systemctl reload gitlab-sidekiq
```

A check of GitLab:

```shell
# sudo  -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV && bundle exec rake gitlab:check RAILS_ENV=production'
```

A local network check:

```shell
# netstat -tulpn | grep LISTEN
```

which should display something like:

```
tcp        0      0 127.0.0.1:25            0.0.0.0:*               LISTEN      25942/exim4
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      9227/nginx: master
tcp        0      0 127.0.0.1:6379          0.0.0.0:*               LISTEN      7032/redis-server 1
tcp        0      0 0.0.0.0:5355            0.0.0.0:*               LISTEN      34/systemd-resolved
tcp        0      0 127.0.0.1:8080          0.0.0.0:*               LISTEN      27084/unicorn_rails
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      34/systemd-resolved
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      24416/sshd
tcp        0      0 127.0.0.1:5432          0.0.0.0:*               LISTEN      24344/postgres
tcp6       0      0 ::1:25                  :::*                    LISTEN      25942/exim4
tcp6       0      0 ::1:6379                :::*                    LISTEN      7032/redis-server 1
tcp6       0      0 :::5355                 :::*                    LISTEN      34/systemd-resolved
tcp6       0      0 :::22                   :::*                    LISTEN      24416/sshd
tcp6       0      0 ::1:5432                :::*                    LISTEN      24344/postgres
```

The relevant parts are:
* 0.0.0.0:80 for NGINX (the reverse proxy);
* 127.0.0.1:8080 for unicorn_rails (the GitLab web server).

A container check from the host server:

```shell
$ nmap -P0 10.0.3.4
...
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp open  http-alt
...
$ wget 10.0.3.4:80
# should save index.html
```

#### IP routing

To simplify SSH access to GitLab, the port has to be forwarded from the host to the container:


```shell
$ sudo iptables -t nat -A PREROUTING -p tcp -m multiport --dports 22 -j DNAT --to-destination 10.0.3.4
$ sudo iptables -t filter -A FORWARD -p tcp -m multiport --dports 22 -j ACCEPT -d 10.0.3.4
$ sudo iptables-save | sudo tee /etc/iptables/rules.v4
$ sudo ip6tables-save | sudo tee /etc/iptables/rules.v6
```

## Finalization

### DNS record

Add the following records in the DOMAIN.TLD zone:

```
gitlab.office                    IN A      IPv4_1
gitlab.office                    IN AAAA   IPv6_2
gitlab.office                    IN CAA    128 issue "letsencrypt.org"
```

### GitLab

#### Setting the first administrator

GitLab allows to sign-in by default and can only disabled from the web interface. A secured connection has also to be used with an SSH tunnel. On your computer:

```shell
user@host ~$ ssh -C -L 1234:10.0.3.4:80 YOUR-ADMIN-LOGIN@office.DOMAIN.TLD
```

And start your web browser at the URL localhost:1234 an try to connect.

In GitLab container:

```shell
# sudo -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV && bundle exec rails console production'

user = User.find_by(email: 'YOUR-LDAP-LOGIN@office.DOMAIN.TLD')
user.admin=true
user.save
```

#### Set visibility and access controls

git clone over HTTP will be disabled :

In *Admin Area*/*Settings*/*Generals*/*Visibility and access controls*:
* set *Enabled Git access protocols* to **Only SSH**.

and save changes.

It does not impact GitLab runners.

#### Disable user sign-up

To prevent account creation.

In *Admin Area*/*Settings*/*Generals*/*Sign-up restrictions*, uncheck:
* *Sign-up enabled*.

and save changes.

#### Disable user sign-in

To prevent non-LDAP connections.

In *Admin Area*/*Settings*/*Generals*/*Sign-in restrictions*, uncheck:
* *Password authentication enabled for web interface*;
* *Password authentication enabled for Git over HTTP(S)*.

and save changes.

#### Disable CI/CD by default

To prevent having useless pipelines for non development repositories.

In *Admin Area*/*Settings*/*CI/DC*, uncheck:
* *Default to Auto DevOps pipeline for all projects*;
* *Enable shared runners for new projects*.

and save changes.

Each runner can be assigned to projects in *Admin Area*/*Overview*/*Runners*.


## Maintenance

You have to be attached to the gitlab container:

```shell
user@local ~$ ssh office
user@remote ~$ sudo /vol/lxc/container-attach gitlab
```

### System

* enabling: `sudo systemctl enable gitlab`
* starting: `sudo systemctl start gitlab`
* stopping: `sudo systemctl stop gitlab`
* configuration check: none
* configuration reload: `sudo systemctl restart gitlab`

### Do a GitLab check

```shell
# sudo  -u gitlab -- sh -c 'cd /usr/share/gitlab && . /etc/gitlab/gitlab-debian.conf && export DB RAILS_ENV && bundle exec rake gitlab:check RAILS_ENV=production'
```

### Backup

```shell
# /vol/scripts/gitlab-backup
```

### Restore

```shell
# /vol/scripts/gitlab-restore
```

## Documentation

[GitLab feature](https://about.gitlab.com/features/)

[GitLab documentation](https://docs.gitlab.com/ce/README.html)

[Debian wiki about GitLab](https://wiki.debian.org/gitlab)

[Debian information about GitLab backup and restoration](https://salsa.debian.org/help/raketasks/backup_restore.md)

[GitLab information about LDAP connection](https://docs.gitlab.com/ce/administration/auth/how_to_configure_ldap_gitlab_ce/index.html)

[How to restrict access to GitLab by LDAP group with LDAP search filter (SO)](https://serverfault.com/questions/874182/how-to-restrict-access-to-gitlab-by-ldap-group-with-ldap-search-filter)

[Debian gitlab package README](/usr/share/doc/gitlab/README.Debian.gz)
