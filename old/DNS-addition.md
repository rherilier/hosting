
for Jenkins DNSrecords:

```
jenkins.office                   IN A      IPv4_1
jenkins.office                   IN AAAA   IPv6_2
jenkins.office                   IN CAA    128 issue "letsencrypt.org"
```
