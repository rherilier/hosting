
# Jenkins

## TODO

* global NGINX reverse proxy.

## System configuration

### Debian's Jenkins

First, add the Jenkins' Debian repositories.

```shell
# apt-get install dirmngr wget
# wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | apt-key add -
# apt-get remove wget
# apt-get autoremove
# cat > /etc/apt/sources.list.d/jenkins.list << EOF
deb https://pkg.jenkins.io/debian-stable binary/
EOF
# apt-get update
```

## Installation

```shell
# apt-get install openjdk-11-jre-headless
# apt-get install jenkins
```

### Configuration

```shell
# vi /etc/default/jenkins
## set HTTP_PORT=8000
# systemctl restart jenkins
```

```shell
user@host ~$ ssh -C -L 1234:10.0.3.5:8000 off
```

#### Plugins installation

And start your web browser at the URL localhost:1234 an try to connect.

a generated password will be asked; it can be found in the container in */var/lib/jenkins/secrets/initialAdminPassword*.

Select *Select plugins to install*.

Select/unselect the following plugins:

* Select *Dashboard view*;
* Select *Build Name and Description Setter*;
* Select *Rebuilder*;
* Unselect *Ant*;
* Unselect *Gradle*;
* Select *xUnit*;
* Unselect *GitHub Branch Source*;
* Unselect *Pipeline: GitHub Groovy Libraries*;
* Select *Pipeline: Stage View*;
* Unselect *Build Pipeline*;
* Select *Conditional BuildStep*;
* Select *Multijob*;
* Select *GitLab*;
* Unselect *Subversion*;
* Unselect *Matrix Authorization Strategy*;
* Unselect *PAM Authentication*;
* Select *Role-based Authorization Strategy*;
* Unselect *Email Extension*;
* Unselect *Mailer*;
* Select *Locale*.

Click on *install*, give information for the first administrator account, and click *next* until the end.

Jenkins will finalize its configuration.

Log in with your administrator account.

#### First settings

Click on *Manage Jenkins* and *Configure System*.

Under *Usage Statistics*:

* uncheck *Help make Jenkins better by sending anonymous usage statistics and crash reports to the Jenkins project.*;
* *Default locale*: en_US;
* uncheck *Ignore browser preference and force this language to all users*.

Under *Jenkins Location*, set:

* *Jenkins URL*: https://jenkins.office.DOMAIN.TLD;
* *System Admin e-mail address*: admin@office.DOMAIN.TLD.

and Save.

#### Access

By setting authentication by LDAP, sign-up will be automatically disabled.

All registered user can manage the instance.

Click on *Manage Jenkins* and *Configure Global Security*.

Under *Security Realm*, select *LDAP* and set:

* *Server*: ldap.office.DOMAIN.TLD
* *root DN*: dc=office,dc=local
* *User search base*: ou=people
* *User search filter*: (&(memberOf=cn=jenkins,ou=service,dc=office,dc=local)(uid={0}))
* *Manager DN*: cn=viewer,ou=system,dc=office,dc=local
* *Manage Password*: get the password from lxc/config/mail/postfix/virtual_mailbox.cf;
* *Display Name LDAP attribute*: displayName
* *Email Address LDAP attribute*: mail

Click on *Test LDAP settings* to check the parameters are good. And Save.

#### LDAP

Add an user named automata and grant it to the gitlab service.


#### GitLab

### Slaves

Add openssh-server to the list of packages to install.





# Backup

```shell
# /vol/scripts/jenkins-backup
```

# Restore

```shell
# /vol/scripts/jenkins-restore
```


## Documentation

[Debian stable on jenkins.io](https://pkg.jenkins.io/debian-stable/)

[Jenkins on gitlab.com](https://docs.gitlab.com/ee/integration/jenkins.html)

[Jenkins behind an NGINX reverse proxy](https://wiki.jenkins.io/display/JENKINS/Jenkins+behind+an+NGinX+reverse+proxy)
