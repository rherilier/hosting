# proxy for Jenkins
#
server {
    listen 443 ssl http2;
    ssl on;

    server_name jenkins.office.DOMAIN.TLD;
    server_tokens off;

    client_body_timeout 1200;
    client_header_timeout 600;

    ssl_certificate /etc/letsencrypt/live/jenkins.office.DOMAIN.TLD/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/jenkins.office.DOMAIN.TLD/privkey.pem;
    ssl_dhparam    /etc/ssl/private/jenkins.office.DOMAIN.TLD/dh2048.pem;

    ssl_session_cache builtin:1000 shared:SSL:10m;
    ssl_prefer_server_ciphers on;
    ssl_protocols TLSv1.3 TLSv1.2;
    ssl_ciphers "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH+aRSA+SHA256 EECDH+aRSA+RC4 EECDH EDH+aRSA HIGH !RC4 !aNULL !eNULL !LOW !3DES !MD5 !EXP !PSK !SRP !DSS !SHA1:!SHA256:!SHA384";

    access_log  /var/log/nginx/jenkins_access.log;
    error_log   /var/log/nginx/jenkins_error.log;

    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_types text/plain text/css text/js text/xml text/javascript application/javascript application/json application/xml application/rss+xml image/svg+xml;

    location / {
        # workaround for https://issues.jenkins-ci.org/browse/JENKINS-45651
        add_header 'X-SSH-Endpoint' 'office.DOMAIN.TLD:50022' always;

        proxy_redirect          off;
        proxy_http_version 1.1;
        proxy_request_buffering off;

        proxy_pass http://10.0.3.5:8000;
        proxy_set_header X-Forwarded-Host $host;
        proxy_set_header X-Forwarded-Server $host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Host $host;
    }
}
