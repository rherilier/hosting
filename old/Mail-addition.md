
### DNS record

```
_submission._tcp.office        IN SRV    1 1 587 mail.office.DOMAIN.TLD.
```

## Configuration

The mail server will provide 4 accesses:
* submission for modern MUA;

add the following line after the smtp one:

```
submission inet n       -       y       -       -       smtpd -o smtpd_tls_security_level=encrypt -o smtpd_sasl_authenticated_header=yes
```

### IP routing

```shell
$ sudo iptables -t nat -A PREROUTING -p tcp -m multiport --dports 587 -j DNAT --to-destination 10.0.3.3
$ sudo iptables -t filter -A FORWARD -p tcp -m multiport --dports 587 -j ACCEPT -d 10.0.3.3
```
