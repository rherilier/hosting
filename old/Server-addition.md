
# Server installation

## GitLab certificate

...

## Jenkins certificate

```shell
$ sudo cp /vol/lxc/nginx/jenkins /etc/nginx/sites-available/
$ sudo ln -s /etc/nginx/sites-available/jenkins /etc/nginx/sites-enabled
$ sudo systemctl reload nginx
```

```shell
$ sudo certbot certonly --nginx -d jenkins.office.DOMAIN.TLD -w /var/www/letsencrypt/
$ sudo su
# chown root:ssl-cert /etc/letsencrypt/archive/jenkins.office.DOMAIN.TLD/*.pem
# chmod 640 /etc/letsencrypt/archive/jenkins.office.DOMAIN.TLD/*.pem
```

Next, DH (Diffie-Hellman) keys have to be created for TLS encryption.

```shell
$ sudo mkdir /etc/ssl/private/jenkins.office.DOMAIN.TLD/
$ sudo openssl dhparam -out /etc/ssl/private/jenkins.office.DOMAIN.TLD/dh2048.pem 2048
$ sudo chmod 644 /etc/ssl/private/jenkins.office.DOMAIN.TLD/dh2048.pem
```

## GitLab

...

### GitLab SSH access from outside

...

## Jenkins

See [Jenkins service setup](Jenkins.md) for Jenkins setup.

When all is fine, enable its reverse proxy.

```shell
$ sudo ln -s /etc/nginx/sites-available/jenkins-ssl /etc/nginx/sites-enabled
$ sudo systemctl reload nginx
```
