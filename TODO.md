
# check that server backup works

daily seems to be OK. Wait and see for weekly and monthly.

# Change contact email for Let's Encrypt

I use admin@office.DOMAIN.TLD instead of a reachable one..

Create an Office 365 diffusion list for admin : sysadm@DOMAIN.TLD

and change the Let's Encrypt account email:

```shell
$ sudo certbot register --update-registration --email sysadm@DOMAIN.TLD
```

To do on host **and** in mail container.

# autodiscover does not work

even with HTTPS...

and still no log entry...

In fact, an AD server seems to be required to do authentication before anything...

Poor Outlook user...
