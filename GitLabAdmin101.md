
# Basic GitLab administration

This document only gives an overview of what have to be done, not how to do it. See corresponding document for precise commands.

## Add a new user

For convenience, it will be named USER.

### LDAP registration

* connect to office.DOMAIN.TLD;
* attach to the *ldap* container;
* add USER to the LDAP base;
* add USER to the needed services (theorically *mail* and *gitlab*);
* add USER to the *all* alias;
* make it enter its password (*pwgen -y 10* to have a minimal ones).

If USER will be an administrator:

* add it to the *admin* alias;
* detach from *ldap* container;
* create an account on office.DOMAIN.TLD using its trigram;
* add USER to the sudo group;
* enable USER's SSH access (need a public key);
* on Office 365, add USER to the *sysadm* mailing list.

### Services

#### Emails

* make it configure its email client;
* do a send/reply mail check;

#### GitLab

* make it connect to gitlab.office.DOMAIN.TLD to check the connectivity.

It will be known by GitLab.

* connect to gitlab.office.DOMAIN.TLD;
* go the *Admin Area*/*User*/*GitLab Admin* and click on *Impersonate*;
* go to *Domain* Group;
* add the new user to this group as *Reporter*;
* add USER as *Developer* for the involved projects;
* stop impersonation.

## GitLab

### Failing jobs on Windows

If a jobs fails on Windows with errors starting with:

```
Test-Path : Acc�s refus�
Au caract�re C:\Users\gitlab-ci\AppData\Local\Temp\build_script186485748\script.ps1:175 : 10
+ } elseif(Test-Path "C:\GitLab-Runner\build\builds\5-1MiFLZ\0\DOMAIN\ ...
+          ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    + CategoryInfo          : PermissionDenied: (C:\GitLab-Runne...\0\DOMAIN\test:String) [Test-Path], UnauthorizedAccessException
    + FullyQualifiedErrorId : ItemExistsUnauthorizedAccessError,Microsoft.PowerShell.Commands.Test PathCommand
```

Restart the jobs and it should pass.

### Notification/MR failures

GitLab may have some difficulties to work after an dependencies update.

Restarting the service should be enough:

```shell
$ user@host$ ssh office
$ sudo /vol/lxc/container-attach gitlab
# systemctl restart gitlab
```

If the problem still exists, restart the container:


```shell
$ user@host$ ssh office
$ sudo /vol/lxc/container-restart gitlab
```

## GitLab Runners

### Stucked jobs

It happens when GitLab does not find any runners.

Make sure that shared runners are enabled in the project's settings (*Settings*/*CI/CD*/Runners).

If the project uses shared runners, see the next point.

### Non starting jobs

When new jobs are added while the runners are already used, the new jobs can stay in *paused* mode.

If the pending jobs do not start, Go the Admin Area, *Overview*/*Runners* and pause/start problematic runners.

If pausing and restarting them does not solve anything, restart the service.

## Restarting services

### Office (Debian)

```shell
$ user@host$ ssh office
$ sudo /vol/lxc/container-attache builder
$ systemctl restart gitlab-runner
```

### Windows

Open a powershell terminal as administrator.

```shell
$ cd \GitLab-Runner
$ .\gitlab-runner.exe restart
```
