
## Default GitLab-CI configuration file

The file is named `.gitlab-ci.yml` and must be present at the repository root.

```yaml
.prepare_git_linux: &git_init_linux
  before_script:
  - cd ..
  - rm -rf ${CI_PROJECT_NAME}
  - eval $(ssh-agent -s)
  - git clone gitlab@gitlab.office.DOMAIN.TLD:${CI_PROJECT_PATH}.git ${CI_PROJECT_NAME}
  - cd ${CI_PROJECT_NAME}
  - git checkout $CI_COMMIT_REF_NAME

.prepare_git_windows: &git_init_windows
  before_script:
  - cd ..
  - rm -R "$env:CI_PROJECT_NAME"
  - git clone "gitlab@gitlab.office.DOMAIN.TLD:$env:CI_PROJECT_PATH.git" "$env:CI_PROJECT_NAME"
  - cd "$env:CI_PROJECT_NAME"
  - git checkout "$env:CI_COMMIT_REF_NAME"

.prepare_build_debug: &build_prepare_debug
  script:
  - mkdir build
  - cd build
  - cmake -DCMAKE_BUILD_TYPE=debug ..

.prepare_build_release: &build_prepare_release
  script:
  - mkdir build
  - cd build
  - cmake -DCMAKE_BUILD_TYPE=relwithdevinfo ..

stages:
  - build
  - test
  - package

build_linux:
  <<: *git_init_linux
  stage: build
  tags:
  - linux
  script:
  - *prepare_build_debug
  - ctest

build_windows:
  <<: *git_init_windows
  stage: build
  tags:
  - windows
  script:
  - *prepare_build_debug
  - make
  - ctest

package_linux:
  stage: package
  tags:
  - linux
  script:
  - *prepare_build_release
  - debuild...
```

## Documentation

[GitLAb-CI in multiple plateforms simultaneously] (https://stackoverflow.com/questions/52031860/gitlab-ci-in-multiple-platforms-simultaneously)

[Same steps for multiple named environments with GitLab-CI](https://stackoverflow.com/questions/44287955/same-steps-for-multiple-named-environments-with-gitlab-ci)

[Special YAML features](https://docs.gitlab.com/ce/ci/yaml/#special-yaml-features)

[YAMP dependencies](https://docs.gitlab.com/ce/ci/yaml/#dependencies)

[GitLab-CI caching](https://docs.gitlab.com/ce/ci/caching/index.html)

[.gitlab-ci.yml on videolan](https://code.videolan.org/videolan/dav1d/blob/master/.gitlab-ci.yml)
